﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YaraSapIntegration.Model;

namespace YaraSapIntegration
{
    public partial class login : Form
    {
        public login()
        {
            InitializeComponent();
        }

        private void buttonAccept_Click(object sender, EventArgs e)
        {
            this.user_login();
        }

        private void textBoxUser_KeyDown(object sender, KeyEventArgs e)
        {
            this.enter_pressed(e);
        }

        private void textBoxPass_KeyDown(object sender, KeyEventArgs e)
        {
            this.enter_pressed(e);
        }

        public void enter_pressed(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.user_login();
            }
        }

        public void user_login()
        {
            if (!String.IsNullOrEmpty(this.textBoxUser.Text) && !String.IsNullOrEmpty(this.textBoxPass.Text))
            {
                User user = (User)new YaraSapIntegration.WebService.WebService().UserLogin(this.textBoxUser.Text, this.textBoxPass.Text);

                if (user != null)
                {
                    MessageBox.Show("Bienvenido:" + user.userId);
                    PrincipalForm principalForm = new PrincipalForm();
                    principalForm.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show(this,"Usuario o contraseña incorrectos","Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show(this, "Debe digitar usuario y contraeña", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
