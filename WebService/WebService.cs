﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using YaraSapIntegration.Model;
using Newtonsoft.Json;

namespace YaraSapIntegration.WebService
{
    class WebService
    {
        private string url = "http://localhost:8080/yara-rest-api-0.0.1-SNAPSHOT/";

        public object UserLogin(string user, string pass)
        {
            string urlLogin = this.url+"user/" + user +"/" + pass;
            User userLogin;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(@urlLogin);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                var json = reader.ReadToEnd();
                userLogin = JsonConvert.DeserializeObject<User>(json);

                //Console.WriteLine(userLogin.userId);
            }
            return userLogin;
        }      

    }
}
